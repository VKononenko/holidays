<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class Holiday extends CBitrixComponent {

    private function CheckD($date, $arHolidays){

        $date_name = date("l",  strtotime($date));

        if($date_name == "Saturday" || $date_name == "Sunday" || in_array($date, $arHolidays)) {
            return false;
        }
        else {
            return $date;
        }

    }


    public function executeComponent() {

        $arUnDate = [];

        // Подключение HighLoadBlock

        if (CModule::IncludeModule('highloadblock')) {
            $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
            $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();
        }

        // Получение данных для проверки уникальности

        $rsData = $strEntityDataClass::getList();

        while ($arItem = $rsData->Fetch()) {
            $arUnDate[] = $arItem['UF_DATE']->format('d.m.Y');
        }


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // Удаление строки

            foreach ($_POST as $key => $value){
                if (strpos($key, 'dl') !== false){
                    $str_id = preg_replace("/[^,.0-9]/", '', $key);
                    $strEntityDataClass::delete($str_id);
                    header('location: ' . $_SERVER['REQUEST_URI']);
                    break;
                }
            }

            // Добавление даты

            if (!empty($_POST['add_date'])){

                $date_add = new DateTime($_POST['add_date']);

                $date_add = $date_add->format('d.m.Y');

                if (!in_array($date_add, $arUnDate)){

                    $arElementFields['UF_DATE'] = $date_add;

                    empty($arElementFields) ?: $strEntityDataClass::add($arElementFields);

                }

            }

            // Проверка  даты

            else if (!empty($_POST['check_date'])){

                $date_check = new DateTime($_POST['check_date']);

                $date_check = $date_check->format('d.m.Y');

                while ($this->CheckD($date_check, $arUnDate) == false){
                    $date_check = (new DateTime($date_check))->modify('+1 day');
                    $date_check = $date_check->format('d.m.Y');
                }

                $this->arResult["DATE_CHECK"] = $date_check;
            }

        }

        // Вывод данных

        $rsData = $strEntityDataClass::getList(array(
                'order' => array('UF_DATE' => 'ASC')
            )
        );

        while ($arItem = $rsData->Fetch()) {
            $this->arResult["DATA"][] = array(
                'ID' => $arItem['ID'],
                'UF_DATE' => $arItem['UF_DATE']->format('d.m.Y')
            );
        }

        $this->includeComponentTemplate();
    }

}
