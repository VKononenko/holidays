<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<style>
    .tbl{
        text-align: center;
        margin: 80px 20px auto;
        display: inline-block;
        vertical-align: top;
    }
</style>

<form method="POST" action="">
    <h2>Добавить праздничную дату:</h2>
    <input type="date" name="add_date" required>
    <input type="submit" value="Добавить">
</form>


<?if (!empty($arResult["DATA"])):?>

    <br><h2>Введенные праздничные даты: </h2>

    <form method="POST" action="">
        <table class="tbl" style="margin-top: 0">
            <tr>
                <td>ID</td>
                <td>ДАТА</td>
                <td>DEL</td>
            </tr>

            <?foreach ($arResult["DATA"] as $item):?>
                <tr>
                    <td><?=$item["ID"]?></td>
                    <td><?=$item["UF_DATE"]?></td>
                    <td class="del_col">
                        <input type="submit" value="Х" title="Удалить дату <?=$item["UF_DATE"]?>" name="dl<?=$item['ID']?>">
                    </td>
                </tr>
            <?endforeach;?>
        </table>
    </form>

<?endif?>

<br><h2>Получение даты ближайшего рабочего дня:  </h2>

<form method="POST" action="">
    <input type="date" name="check_date" required>
    <input type="submit" value="Узнать">
</form>

<h1 style="color: forestgreen"><?=$arResult["DATE_CHECK"]?></h1>